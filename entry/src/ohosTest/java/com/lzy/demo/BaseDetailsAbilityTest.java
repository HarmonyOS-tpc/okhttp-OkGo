package com.lzy.demo;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.BitmapCallback;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.convert.StringConvert;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.utils.OkLogger;
import com.lzy.okrx2.adapter.ObservableResponse;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import ohos.media.image.PixelMap;
import ohos.utils.zson.ZSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class BaseDetailsAbilityTest {

    @Test
    public void getMethod() {
        OkGo.<String>get("https://httpbin.org/get")
                .tag(this)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        OkLogger.e("GET响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }
                });
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void postMethod() {
        // 测试POST 请求
        OkGo.<String>post("https://httpbin.org/post")
                .params("username", "zhouxin8357")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        OkLogger.e("POST请求响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteMethod() {
        // 测试DELETE 请求
        OkGo.<String>delete("https://httpbin.org/delete")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        OkLogger.e("DELETE请求响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void putMethod() {
        // 测试PUT请求
        OkGo.<String>put("https://httpbin.org/put")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        OkLogger.e("PUT请求响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void patchMethod() {
        // 测试PATCH请求
        OkGo.<String>patch("https://httpbin.org/patch")
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        OkLogger.e("PATCH请求响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void downloadMethod() {
        // 测试DOWNLOAD请求
        OkGo.<PixelMap>get("https://httpbin.org/image/webp")
                .tag(this)
                .headers("header1", "headerValue1")
                .params("param1", "paramValue1")
                .execute(new BitmapCallback() {
                    @Override
                    public void onSuccess(Response<PixelMap> response) {
                        Assert.assertTrue(response.body().getImageInfo().size.height > 0);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void uploadJsonMethod() {
        Map<String, Object> params = new HashMap<>();
        params.put("key1", "value1");
        params.put("key2", "这里是需要提交的json格式数据");
        params.put("key3", "也可以使用三方工具将对象转成json字符串");
        params.put("key4", "其实你怎么高兴怎么写都行");
        ZSONObject jsonObject = new ZSONObject(params);
        OkGo.<String>post("https://httpbin.org/post")
                .tag(this)
                .headers("header1", "headerValue1")
                .upJson(jsonObject)
                .execute(new StringCallback() {
                    @Override
                    public void onSuccess(Response<String> response) {
                        OkLogger.e("upJson请求响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void rx2PostMethod() {
        OkGo.<String>post("https://httpbin.org/post")
                .headers("aaa", "111")
                .upString("19190520")
                .converter(new StringConvert())
                .adapt(new ObservableResponse<String>())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {

                    }
                })
                .observeOn(com.lzy.okrx2.utils.HosSchedulers.mainThread())
                .subscribe(new Observer<Response<String>>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(Response<String> response) {
                        OkLogger.e("Rx2上传string请求响应:---" + response.body());
                        Assert.assertTrue(response.body().length() > 0);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        OkLogger.e(throwable.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}