/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzy.demo.data;

import com.lzy.demo.ResourceTable;
import com.lzy.demo.model.MainItemModel;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * ListHolder
 */
public class ListHolder {
    private static final String TAG = "ListHolder";

    private Ability mSlice;
    private ComponentContainer mRootLayout;
    private ListContainer listContainer;
    private ListItemProvider listItemProvider;
    private ListContainer.ItemClickedListener itemClickedListener;

    public ListHolder(Ability abilitySlice, ArrayList<MainItemModel> data, ListContainer.ItemClickedListener itemClickedListener) {
        mSlice = abilitySlice;
        listItemProvider = new ListItemProvider(abilitySlice, data);
        this.itemClickedListener = itemClickedListener;
    }

    /**
     * 创建ComponentContainer
     *
     * @return ComponentContainer
     */
    public ComponentContainer createComponent() {
        Component mainComponent = LayoutScatter.getInstance(mSlice).parse(ResourceTable.Layout_layout_main_ok_go, null, false);
        if (!(mainComponent instanceof ComponentContainer)) {
            return null;
        }
        if (mainComponent instanceof ComponentContainer) {
            mRootLayout = (ComponentContainer) mainComponent;
            listContainer = (ListContainer) mRootLayout.findComponentById(ResourceTable.Id_listView);
            listContainer.setItemProvider(listItemProvider);
            if (itemClickedListener != null) {
                listContainer.setItemClickedListener(itemClickedListener);
            }
            return mRootLayout;
        }
        if (mRootLayout.findComponentById(ResourceTable.Id_listView) instanceof ListContainer) {
            listContainer = (ListContainer) mRootLayout.findComponentById(ResourceTable.Id_listView);
            listContainer.setItemProvider(listItemProvider);
            if (itemClickedListener != null) {
                listContainer.setItemClickedListener(itemClickedListener);
            }
            return mRootLayout;
        }
        listContainer.setItemProvider(listItemProvider);
        if (itemClickedListener != null) {
            listContainer.setItemClickedListener(itemClickedListener);
        }
        return mRootLayout;
    }
}

