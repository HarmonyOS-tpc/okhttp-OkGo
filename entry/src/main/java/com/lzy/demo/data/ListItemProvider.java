/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzy.demo.data;

import com.lzy.demo.ResourceTable;
import com.lzy.demo.model.MainItemModel;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.Text;

import java.util.ArrayList;

/**
 * 适配器
 */
public class ListItemProvider extends RecycleItemProvider {
    private ArrayList<MainItemModel> data;
    private Ability mSlice;

    ListItemProvider(Ability abilitySlice, ArrayList<MainItemModel> data) {
        mSlice = abilitySlice;
        this.data = data;
    }

    @Override
    public long getItemId(int i1) {
        return 0;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i1) {
        return data.get(i1);
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        Component component = LayoutScatter.getInstance(mSlice).parse(ResourceTable.Layout_item_main_list, null, false);
        if (!(component instanceof ComponentContainer)) {
            return null;
        }
        ComponentContainer rootLayout = (ComponentContainer) component;
        Text leftText = (Text) rootLayout.findComponentById(ResourceTable.Id_title_content);
        leftText.setText(data.get(position).getTitle());
        Text rightText = (Text) rootLayout.findComponentById(ResourceTable.Id_desc_content);
        rightText.setText(data.get(position).getDes());
        return component;
    }
}
