/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzy.demo;

import com.lzy.demo.data.ListHolder;
import com.lzy.demo.model.MainItemModel;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.ElementName;

import java.util.ArrayList;

/**
 * 主ability
 */
public class MainAbility extends Ability {
    private ListHolder listHolder;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        final ArrayList<MainItemModel> data = new ArrayList<>();

        // OkGo 基础版本的使用
        data.add(new MainItemModel("OkGo基本功能",
                "1.GET，HEAD，OPTIONS，POST，PUT，DELETE, PATCH, TRACE 请求方法演示" +
                        "2.请求服务器返回bitmap对象" +
                        "3.支持https请求" +
                        "4.支持同步请求" +
                        "5.支持301重定向"));

        // Rx2版本库的使用
        data.add(new MainItemModel("Rx2基本功能",
                "1.支持GET，HEAD，OPTIONS，POST，PUT，DELETE, PATCH, TRACE 8种请求方式" +
                        "2.自动解析JSONObject对象" +
                        "3.自动解析JSONArray对象" +
                        "4.上传string文本" +
                        "5.上传json数据"));

        // 基于OkGo的下载扩展库的使用
        data.add(new MainItemModel("OkServer下载文件",
                "1. 这个属于OkServer依赖中的功能,并不属于OkGo" +
                        "2. 支持断点下载和状态管理" +
                        "3. 支持自定义下载任务优先级" +
                        "4. 支持链试调用" +
                        "5. 最多支持扩展3个额外数据" +
                        "6. 支持同时指定多个下载目录"));

        listHolder = new ListHolder(this, data, (listContainer, component, i1, l1) -> goToDetails(i1));

        setUIContent(listHolder.createComponent());
    }

    private void goToDetails(int i1) {
        Intent intent = Intent.makeMainAbility(ElementName.createRelative("com.lzy.demo", "com.lzy.demo.BaseDetailsAbility", ""));
        intent.setParam("ActionType", i1);
        startAbility(intent);
    }
}
