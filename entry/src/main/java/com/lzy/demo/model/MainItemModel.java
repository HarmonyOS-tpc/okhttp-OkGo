/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzy.demo.model;

/**
 * 列表ItemModel
 */
public class MainItemModel {
    private String title;
    private String des;
    private int type;

    public MainItemModel() {
    }

    public String getTitle() {
        return title;
    }

    public String getDes() {
        return des;
    }

    public int getType() {
        return type;
    }

    public MainItemModel(String title, String des) {
        this.title = title;
        this.des = des;
    }

    public MainItemModel(String title, String des, int type) {
        this.title = title;
        this.des = des;
        this.type = type;
    }
}
