/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzy.demo.utils;

import com.lzy.okgo.utils.OkLogger;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 图片处理工具类
 */
public class ImageUtil {
    private static final String IMAGE_TYPE = "image/*";
    private static final int IO_END_LEN = -1;
    private static final int CACHE_SIZE = 256 * 1024;
    private static final String LABEL = "ImageUtils";

    private ImageUtil() {
    }

    /**
     * * getPixelMapByUri
     *
     * @param uri uri
     * @return PixelMap pixelMap
     */
    public static PixelMap getPixelMapByUri(String uri) {
        byte[] data = read(uri);
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        srcOpts.formatHint = IMAGE_TYPE;
        ImageSource imageSource = ImageSource.create(data, srcOpts);

        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;

        return imageSource.createPixelmap(decodingOpts);
    }

    /**
     * 把File 转成 PixelMap
     *
     * @param file 文件
     * @return PixelMap 返回位图
     */
    public static PixelMap getPixelMapByFile(File file) {
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        srcOpts.formatHint = IMAGE_TYPE;
        ImageSource imageSource = ImageSource.create(file.getAbsoluteFile(), srcOpts);

        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;
        return imageSource.createPixelmap(decodingOpts);
    }

    /**
     * * Read File
     *
     * @param filePath filePath
     * @return byte array
     */
    public static byte[] read(String filePath) {
        FileInputStream fileInputStream = null;
        byte[] cache = new byte[CACHE_SIZE];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] result = new byte[0];
        int len = IO_END_LEN;
        try {
            fileInputStream = new FileInputStream(new File(filePath));
            len = fileInputStream.read(cache);
            while (len != IO_END_LEN) {
                baos.write(cache, 0, len);
                len = fileInputStream.read(cache);
            }
            result = baos.toByteArray();
        } catch (IOException e) {
            OkLogger.e("read data from file failed");
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                OkLogger.e("close file input stream failed");
            }

            try {
                baos.close();
            } catch (IOException e) {
                OkLogger.d(e.getMessage());
            }
        }
        return result;
    }

}
