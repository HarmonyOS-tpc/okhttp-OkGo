/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lzy.demo.utils;

import com.lzy.okgo.model.Progress;
import com.lzy.okgo.utils.OkLogger;
import com.lzy.okserver.download.DownloadListener;
import ohos.agp.components.Image;
import ohos.agp.components.ProgressBar;

import java.io.File;

/**
 * ================================================
 * 作    者：jeasonlzy（廖子尧）
 * 版    本：1.0
 * 创建日期：2017/6/7
 * 描    述：
 * 修订历史：
 * ================================================
 */
public class LogDownloadListener extends DownloadListener {

    private Image downImage;
    private ProgressBar progressBar;

    public LogDownloadListener(Image downImage, ProgressBar progressBar) {
        super("LogDownloadListener");
        this.downImage = downImage;
        this.progressBar = progressBar;
    }

    @Override
    public void onStart(Progress progress) {
        OkLogger.e("onStart: " + progress);
    }

    @Override
    public void onProgress(Progress progress) {
        OkLogger.e("onProgress: " + progress);
        progressBar.setProgressValue((int) (1.0f * progress.currentSize / progress.totalSize * 100));
    }

    @Override
    public void onError(Progress progress) {
        OkLogger.e("onError: " + progress.exception.getMessage());
    }

    @Override
    public void onFinish(File file, Progress progress) {
        OkLogger.e("onFinish: " + progress);
    }

    @Override
    public void onRemove(Progress progress) {
        OkLogger.e("onRemove: " + progress);
    }
}
