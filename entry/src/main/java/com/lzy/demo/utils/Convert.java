/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lzy.demo.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import java.io.Reader;
import java.lang.reflect.Type;

/**
 * ================================================
 * 作    者：jeasonlzy（廖子尧）
 * 版    本：1.0
 * 创建日期：16/9/28
 * 描    述：
 * 修订历史：
 * ================================================
 */
public class Convert {

    /**
     * 创建
     * @return Gson
     */
    private static Gson create() {
        return GsonHolder.gson;
    }

    /**
     * 构造
     */
    private static class GsonHolder {
        private static Gson gson = new Gson();
    }

    /**
     * 从json到其他类型
     *
     * @param json String
     * @param type 类型
     * @param <T> 泛型
     * @return 泛型
     * @throws JsonIOException 异常
     * @throws JsonSyntaxException 异常
     */
    public static <T> T fromJson(String json, Class<T> type) throws JsonIOException, JsonSyntaxException {
        return create().fromJson(json, type);
    }

    /**
     * 从json到其他类型
     *
     * @param json string
     * @param type 类型
     * @param <T> 泛型
     * @return 泛型
     */
    public static <T> T fromJson(String json, Type type) {
        return create().fromJson(json, type);
    }

    /**
     * 从json到其他类型
     *
     * @param reader json解析
     * @param typeOfT 类型
     * @param <T> 泛型
     * @return 泛型
     * @throws JsonIOException 异常
     * @throws JsonSyntaxException 异常
     */
    public static <T> T fromJson(JsonReader reader, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return create().fromJson(reader, typeOfT);
    }

    /**
     * 从json到其他类型
     *
     * @param json jsonString
     * @param classOfT class
     * @param <T> 泛型
     * @return 泛型
     * @throws JsonSyntaxException 异常
     * @throws JsonIOException 异常
     */
    public static <T> T fromJson(Reader json, Class<T> classOfT) throws JsonSyntaxException, JsonIOException {
        return create().fromJson(json, classOfT);
    }

    /**
     * 从json转到其他类型
     *
     * @param json json
     * @param typeOfT 类型
     * @param <T> 泛型
     * @return 泛型
     * @throws JsonIOException 异常
     * @throws JsonSyntaxException 异常
     */
    public static <T> T fromJson(Reader json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return create().fromJson(json, typeOfT);
    }

    /**
     * 转json
     *
     * @param src 参数
     * @return String
     */
    public static String toJson(Object src) {
        return create().toJson(src);
    }

    /**
     * 转json
     *
     * @param src 对象
     * @param typeOfSrc 类型
     * @return String
     */
    public static String toJson(Object src, Type typeOfSrc) {
        return create().toJson(src, typeOfSrc);
    }
}
