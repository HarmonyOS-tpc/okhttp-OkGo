/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lzy.okgo.db;

import com.lzy.okgo.utils.OkLogger;
import ohos.data.rdb.RdbStore;
import ohos.data.resultset.ResultSet;

/**
 * ================================================
 * 作    者：jeasonlzy（廖子尧）
 * 版    本：1.0
 * 创建日期：2017/5/11
 * 描    述：
 * 修订历史：
 * ================================================
 */
public class DBUtils {

    public static boolean isNeedUpgradeTable(RdbStore db, TableEntity table) {
        if (!isTableExists(db, table.tableName)) return true;

        ResultSet cursor = db.querySql("select * from " + table.tableName, null);
        if (cursor == null) return false;
        try {
            int columnCount = table.getColumnCount();
            if (columnCount == cursor.getColumnCount()) {
                for (int i = 0; i < columnCount; i++) {
                    if (table.getColumnIndex(cursor.getColumnNameForIndex(i)) == -1) {
                        return true;
                    }
                }
            } else {
                return true;
            }
            return false;
        } finally {
            cursor.close();
        }
    }

    public static boolean isTableExists(RdbStore db, String tableName) {
        if (tableName == null || db == null || !db.isOpen()) return false;

        ResultSet cursor = null;
        int count = 0;
        try {
            cursor = db.querySql("select count(*) from sqlite_master where type = ? and name = ?", new String[]{"table", tableName});
            if (!cursor.goToFirstRow()) {
                return false;
            }
            count = cursor.getInt(0);
        } catch (Exception e) {
            OkLogger.printStackTrace(e);
        } finally {
            if (cursor != null) cursor.close();
        }
        return count > 0;
    }

    public static boolean isFieldExists(RdbStore db, String tableName, String fieldName) {
        if (tableName == null || db == null || fieldName == null || !db.isOpen()) return false;

        ResultSet cursor = null;
        try {
            cursor = db.querySql("select * from " + tableName + " limit 0", null);
            return cursor != null && cursor.getColumnIndexForName(fieldName) != -1;
        } catch (Exception e) {
            OkLogger.printStackTrace(e);
            return false;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
