/*
 * Copyright 2016 jeasonlzy(廖子尧)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lzy.okgo.cookie.store;

import java.util.List;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * ================================================
 * 作    者：jeasonlzy（廖子尧）
 * 版    本：1.0
 * 创建日期：2016/1/14
 * 描    述：CookieStore 的公共接口
 * 修订历史：
 * ================================================
 */
public interface CookieStore {

    void saveCookie(HttpUrl url, List<Cookie> cookie);

    void saveCookie(HttpUrl url, Cookie cookie);

    List<Cookie> loadCookie(HttpUrl url);

    List<Cookie> getAllCookie();

    List<Cookie> getCookie(HttpUrl url);

    boolean removeCookie(HttpUrl url, Cookie cookie);

    boolean removeCookie(HttpUrl url);

    boolean removeAllCookie();
}
